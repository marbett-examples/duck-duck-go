function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
    urlDuckDuckGo: 'https://api.duckduckgo.com/'
  }
  if (env == 'dev') {
    // customize
    config.urlDuckDuckGo = 'https://api.duckduckgo.com/'
  } else if (env == 'e2e') {
    // customize
    config.urlDuckDuckGo = 'https://api.duckduckgo.com/'
  }
  return config;
}