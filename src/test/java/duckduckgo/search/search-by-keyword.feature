Feature: Search by keyword, then verify the source and url obtained
  for help, see: https://api.duckduckgo.com/api
  
  Scenario Outline: Search by keyword '<keyword>' and format '<format>'
    Given url urlDuckDuckGo
    And param q = '<keyword>'
    And param format = '<format>'
    When method get
    Then status 200
    And match $response.AbstractSource == '<abstractSource>'
    * def abstractUrl = $response.AbstractURL
    * print 'abstractUrl is : ', abstractUrl

    Given url abstractUrl
    When method get
    Then status 200

  Examples:
    | keyword | format | abstractSource |
    | Toledo  | json   | Wikipedia      |
